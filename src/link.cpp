#include "main.hpp"

#include <cmath>

Link::Link(PMat *m1, PMat *m2, double k, double z)
{
	init(m1, m2, k, z);
}

void Link::init(PMat *m1, PMat *m2, double k, double z)
{
	this->m1 = m1;
	this->m2 = m2;
	this->k = k;
	this->z = z;
	if (m1 != NULL && m2 != NULL)
		this->l = G3Xdist(m1->pos, m2->pos);
}

#define EPSILON 0.0000001

void Link::hook()
{
	if (m1 == NULL || m2 == NULL) return;
    double d = G3Xdist(m1->pos, m2->pos);
    if (d < EPSILON) return;

    double f = k * (1 - l/d);
	
    G3Xvector m1m2 = {m2->pos[0] - m1->pos[0], m2->pos[1] - m1->pos[1], m2->pos[2] - m1->pos[2]};

    m1->frc[0] += f * m1m2[0] - z * m1m2[0];
    m1->frc[1] += f * m1m2[1] - z * m1m2[1];
    m1->frc[2] += f * m1m2[2] - z * m1m2[2];

    m2->frc[0] -= (f * m1m2[0] - z * m1m2[0]);
    m2->frc[1] -= (f * m1m2[1] - z * m1m2[1]);
    m2->frc[2] -= (f * m1m2[2] - z * m1m2[2]);
}

void Link::draw()
{
	if (m1 == NULL || m2 == NULL) return;
	glBegin(GL_LINES);
	glColor3f(.8,.7,.6);
	glVertex3f(m1->pos[0], m1->pos[1], m1->pos[2]);
	glVertex3f(m2->pos[0], m2->pos[1], m2->pos[2]);
	glEnd();
}
