#include "main.hpp"

#include "iostream"
#include "stdlib.h"
#include "cstdlib"

PMat::PMat(G3Xpoint pos, double m)
{
    init(pos, m);
}

void PMat::init(G3Xpoint pos, double m)
{
	this->pos[0] = pos[0];
	this->pos[1] = pos[1];
	this->pos[2] = pos[2];
	
	this->m = m;
	
	this->frc[0] = this->frc[1] = this->frc[2] = 
    this->vit[0] = this->vit[1] = this->vit[2] = 0.d;
}

MasseLibre::MasseLibre(G3Xpoint pos, double m): PMat(pos, m)
{
	ray = .1d;
	col[3] = 1.;
	col[1] = col[2] = .1;
	col[0] = .9;
}

PointFixe::PointFixe(G3Xpoint pos): PMat(pos, 1.d)
{
	ray = .2d;
	col[3] = 1.;
	col[0] = col[1] = .1;
	col[2] = .3;
}

void MasseLibre::setup()
{
    vit[0] += dt/m * frc[0];
    vit[1] += dt/m * frc[1];
    vit[2] += dt/m * frc[2];
	
    pos[0] += dt * vit[0];
    pos[1] += dt * vit[1];
    pos[2] += dt * vit[2];

    frc[0] = frc[1] = frc[2] = 0.d;
}

void PointFixe::setup()
{
    frc[0] = frc[1] = frc[2] = 0.d;
}

void PMat::draw()
{
    glColor3f(col[0],col[1],col[2]);
    glPushMatrix();
    glTranslatef(pos[0], pos[1], pos[2]);
    glutSolidSphere(ray, 10, 10);
    glPopMatrix();
}
