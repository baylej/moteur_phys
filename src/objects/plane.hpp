#ifndef PLANE_HPP
#define PLANE_HPP

#include "object.hpp"

class Plane : public Object
{
	int sideSize;
	double gapSize;
public:
	Plane(int sideSize = 5, double gapSize = 1.d);
	
	void init();
};

#endif // PLANE_HPP
