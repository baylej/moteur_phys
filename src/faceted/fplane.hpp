#ifndef FPLANE_HPP
#define FPLANE_HPP
#pragma once

#include "faceted.hpp"

class FacetedPlane : public Faceted
{
	double sideSize;
public:
	FacetedPlane(double sideSize=1.);
	void init();
};

#endif // FPLANE_HPP
