#include "../main.hpp"
#include "cube.hpp"

Cube::Cube(double sideSize)
{
	this->sizeSize = sideSize;
	this->f = 2*6;
	init();
}

void Cube::init() {
	if (this->Facets == NULL)
		this->Facets = new Facet[this->f];

	double d = this->sizeSize/2.d;

	Facet fa;
	fa.a[0] =  -d;
	fa.a[1] =   d;
	fa.a[2] = 0.d;

	fa.b[0] =   d;
	fa.b[1] =   d;
	fa.b[2] = 0.d;

	fa.c[0] =   d;
	fa.c[1] =  -d;
	fa.c[2] = 0.d;


	Facet fb;
	fb.a[0] =  -d;
	fb.a[1] =   d;
	fb.a[2] = 0.d;

	fb.b[0] =   d;
	fb.b[1] =  -d;
	fb.b[2] = 0.d;

	fb.c[0] =  -d;
	fb.c[1] =  -d;
	fb.c[2] = 0.d;

	G3Xhmat trans; G3Xloadidentity(trans);
	g3x_MakeTranslationXYZ(trans, 0.d, 0.d, d);

	G3Xhmat rot; G3Xloadidentity(rot);

	G3Xhmat mat[6];
	G3Xcopymat(mat[0], trans);
	g3x_MakeRotationX(rot,  PI);   g3x_ProdHMat(rot, trans, mat[1]);
	g3x_MakeRotationY(rot,  PI_2); g3x_ProdHMat(rot, trans, mat[2]);
	g3x_MakeRotationY(rot, -PI_2); g3x_ProdHMat(rot, trans, mat[3]);
	g3x_MakeRotationX(rot,  PI_2); g3x_ProdHMat(rot, trans, mat[4]);
	g3x_MakeRotationX(rot, -PI_2); g3x_ProdHMat(rot, trans, mat[5]);

	for (int i=0; i<12; i+=2) {
		g3x_ProdHMatPoint(mat[i/2], fa.a, this->Facets[i].a);
		g3x_ProdHMatPoint(mat[i/2], fa.b, this->Facets[i].b);
		g3x_ProdHMatPoint(mat[i/2], fa.c, this->Facets[i].c);

		g3x_ProdHMatPoint(mat[i/2], fb.a, this->Facets[i+1].a);
		g3x_ProdHMatPoint(mat[i/2], fb.b, this->Facets[i+1].b);
		g3x_ProdHMatPoint(mat[i/2], fb.c, this->Facets[i+1].c);
	}

	this->facetColor[0] = .55d;
	this->facetColor[1] = .11d;
	this->facetColor[2] = .22d;
}
