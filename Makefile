CXX= g++
CFLAGS= -W -Wall -Wpointer-arith -g -c -std=c++11
LFLAGS= -lm -lGL -lGLU -lglut -lg3x.64b
SRCDIR= src
SOURCES= $(wildcard $(SRCDIR)/*.cpp $(SRCDIR)/*/*.cpp)
OBJECTS= $(SOURCES:.cpp=.o)
TARGET= simulator

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) -o $@ $^ $(LFLAGS)

%.o: %.cpp
	$(CXX) -o $@ $(CFLAGS) $^

.PHONY: clean mrproper

clean:
	rm $(SRCDIR)/*.o
	rm $(SRCDIR)/*/*.o

mrproper: clean
	rm $(TARGET)

