#include <cmath>

#include "main.hpp"

using std::fabs;

Collision::Collision(Object *a, Object *b, double thres, double linkLen)
{
	this->thres = thres;
	this->linkLen = linkLen;
	this->a = a;
	this->b = b;
	int tot_a = a->p + a->m;
	int tot_b = b->p + b->m;
	this->l = tot_a * tot_b;
	this->CondLinks = new CondLink[this->l];
	
	for (int i=0; i<tot_a; i++) {
		for (int j=0; j<tot_b; j++) {
			Link *linkp = new Link(&(a->Points[i]), &(b->Points[j]));
			linkp->l = this->linkLen;
			CondLinks[i*tot_b+j].init(linkp, this->thres);
		}
	}
}

void Collision::hook()
{
	for (int i=0; i<this->l; i++) {
		CondLinks[i].hook();
	}
}

void Collision::draw()
{
	for (int i=0; i<this->l; i++) {
		CondLinks[i].draw();
	}
}

// ----

static void computeNextPosition(G3Xpoint res, PMat *point)
{
	G3Xvector vit; G3Xcopy(vit, point->vit);
	vit[0] += dt/point->m * point->frc[0];
	vit[1] += dt/point->m * point->frc[1];
	vit[2] += dt/point->m * point->frc[2];

	G3Xcopy(res, point->pos);
	res[0] += dt * vit[0];
	res[1] += dt * vit[1];
	res[2] += dt * vit[2];
}

static void computeFacetNormal(G3Xvector res, Facet *facet)
{
	// Facets are front clockwise
	G3Xvector ac; G3Xsetvct(ac, facet->a, facet->c);
	G3Xvector ab; G3Xsetvct(ab, facet->a, facet->b);

	G3Xprodvct(res, ac, ab);
	G3Xnormalize(res);
}

#define IMAX3(a,b,c) (((a)>(b))?(((a)>(c))?(0):(2)):(((b)>(c))?(1):(2)))

static bool isInside(G3Xpoint m, Facet *facet, G3Xvector facet_n)
{
	G3Xvector ma, mb, mc;
	G3Xsetvct(ma, m, facet->a);
	G3Xsetvct(mb, m, facet->b);
	G3Xsetvct(mc, m, facet->c);

	G3Xvector mamb, mbmc, mcma;
	G3Xprodvct(mamb, ma, mb);
	G3Xprodvct(mbmc, mb, mc);
	G3Xprodvct(mcma, mc, ma);

	double va = G3Xprodscal(mamb, facet_n);
	double vb = G3Xprodscal(mbmc, facet_n);
	double vc = G3Xprodscal(mcma, facet_n);

	return (va >= 0 && vb >= 0 && vc >= 0)
	    || (va <= 0 && vb <= 0 && vc <= 0);
}

/// decompose selon `n` le vecteur `v` en un vecteur normal `vn`, et tangentiel `vt`
static void decomposeVect(G3Xvector res_vn, G3Xvector res_vt, G3Xvector v, G3Xvector n)
{
	double vpsn = G3Xprodscal(v, n);
	G3Xcopy(res_vn, n); G3Xmulvct(res_vn, vpsn);
	G3Xcopy(res_vt, v); G3Xsubvct(res_vt, res_vn);
}

static void computeDynInv(G3Xvector res, G3Xvector vn, G3Xvector vt, double dissipation, double rubbing)
{
	double n_vn = G3Xvnorm(vn);
	double n_vt = G3Xvnorm(vt);

	G3Xcopy(res, vn);
	G3Xmulvct(res, -dissipation);

	if (!(n_vt < rubbing*n_vn)) {
		G3Xvector comp;
		G3Xcopy(comp, vt);
		G3Xmulvct(comp, 1.d-(rubbing * n_vn/n_vt));
		G3Xaddvct(res, comp);
	}
}

void FacetedCollision::hook()
{
	for (int m=0; m<this->a->m; m++) {
		for (int f=0; f<this->b->f; f++) {
			G3Xpoint  a;  G3Xcopy(a, this->b->Facets[f].a);
			G3Xpoint  p;  G3Xcopy(p, this->a->MassesLibres[m].pos);
			G3Xpoint  q;  computeNextPosition(q, &this->a->MassesLibres[m]);
			G3Xvector n;  computeFacetNormal(n, &this->b->Facets[f]);

			G3Xvector pq; G3Xsetvct(pq, p, q);
			G3Xvector ap; G3Xsetvct(ap, a, p);

			double nap = G3Xprodscal(n, ap);
			double npq = G3Xprodscal(n, pq);

			// le point ne se dirige pas vers le plan de la facette
			if (!(nap > 0 && npq < 0))
				continue;

			double u = -nap/npq;

			// le point ne s'apprete pas a traverser le plan de la facette
			if (!(u<=1))
				continue;

			G3Xpoint _m; G3Xcopy(_m, pq); G3Xmulvct(_m, u); G3Xaddvct(_m, p);

			// le point d'intersection avec la facette n'est pas dans la facette
			if (!isInside(_m, &this->b->Facets[f], n))
				continue;
			// this->a->MassesLibres[m] est sur le point d'heurter this->b->Facets[f]

			G3Xvector fn, ft; decomposeVect(fn, ft, this->a->MassesLibres[m].frc, n);
			G3Xvector vn, vt; decomposeVect(vn, vt, this->a->MassesLibres[m].vit, n);

			G3Xvector frc;
			computeDynInv(frc, fn, ft, this->dissipation, this->rubbing);
			G3Xcopy(this->a->MassesLibres[m].frc, frc);

			G3Xvector vit;
			computeDynInv(vit, vn, vt, this->dissipation, this->rubbing);
			G3Xcopy(this->a->MassesLibres[m].vit, vit);

			// evite les effets d'une collision eventuelle avec une seconde facette
			break;
		}
	}
}

FacetedCollision::FacetedCollision(Object *a, Faceted *b, double d, double r)
{
	if (d<0 || d>1 || r<0 || r>1)
		throw 1;

	this->dissipation = d;
	this->rubbing = r;

	this->a = a;
	this->b = b;
}

GeometryCollision::GeometryCollision(Object *a, Geometry *g)
{
	this->a = a;
	this->g = g;
}

void GeometryCollision::hook()
{
	G3Xpoint p;
	G3Xvector fb, tfb;
	for (int i=0; i<this->a->m; i++) {
		g3x_ProdHMatPoint(this->g->inverse, this->a->MassesLibres[i].pos, p);
		if (this->g->feedback(p, fb)) {
			g3x_ProdHMatVector(this->g->direct, fb, tfb);
			G3Xmulvct(tfb, 2000);
			G3Xcopy(this->a->MassesLibres[i].frc, tfb);
			// Triche ici car la vitesse de la masseLibre est assez importante
			G3Xvector fn, ft; decomposeVect(fn, ft, this->a->MassesLibres[i].vit, tfb);
			if (G3Xprodscal(fn, tfb) < 0)
				G3Xzero(this->a->MassesLibres[i].vit);
		}
	}
}
