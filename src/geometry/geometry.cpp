#include "../main.hpp"
#include "geometry.hpp"

Geometry::Geometry()
{
	G3Xloadidentity(direct);
	G3Xloadidentity(inverse);
}

void Geometry::draw()
{
	glColor3f(.2, .45, .33);
	glPushMatrix();
    glTranslatef(direct[12], direct[13], direct[14]);
    glutSolidSphere(.2, 10, 10);
    glPopMatrix();
}
