#ifndef MAIN_H
#define MAIN_H
#pragma once

#include <g3x.h>

#ifndef PI_2
	#define PI_2 1.5707963267948966
#endif
#ifndef PI_4
	#define PI_4 0.7853981633974483
#endif
#ifndef PI_6
	#define PI_6 0.5235987755982988
#endif
#ifndef PI_8
	#define PI_8 0.39269908169872414
#endif

extern double dt;
extern G3Xvector def_v;

#include "pmat.hpp"
#include "link.hpp"
#include "condlink.hpp"

#include "objects/object.hpp"
#include "faceted/faceted.hpp"
#include "geometry/geometry.hpp"

#include "collision.hpp"

#endif
