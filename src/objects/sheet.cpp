#include "../main.hpp"
#include "sheet.hpp"

Sheet::Sheet(int sideSize): sideSize(sideSize)
{
	this->m = sideSize * sideSize;
	this->l = m*4;
	if (sideSize < 3) throw 1;
	init();
}

void Sheet::init()
{
	if (this->MassesLibres == NULL)
		this->MassesLibres = new MasseLibre[this->m];
	if (this->Links == NULL)
		this->Links = new Link[this->l];
	
	int d = sideSize / 2;
	
	for (int i=0; i<sideSize; i++) {
		for (int j=0; j<sideSize; j++) {
			G3Xpoint pnt = {(double)i-d, (double)j-d, 0.};
			this->MassesLibres[i*sideSize+j].init(pnt);
		}
	}

	for (int i=0; i<sideSize; i++) {
		for (int j=0; j<sideSize; j++) {
			if (j<sideSize-1)
				Links[i*4*sideSize+j*4  ].init(&Points[i*sideSize+j], &Points[    i*sideSize+j+1], 10000.d, 100.d);
			if (i<sideSize-1)
				Links[i*4*sideSize+j*4+1].init(&Points[i*sideSize+j], &Points[(i+1)*sideSize+j]  , 10000.d, 100.d);
			if (j<sideSize-1 && i<sideSize-1)
				Links[i*4*sideSize+j*4+2].init(&Points[i*sideSize+j], &Points[(i+1)*sideSize+j+1], 10000.d, 100.d);
			if (j<sideSize-1 && i<sideSize-1)
				Links[i*4*sideSize+j*4+3].init(&Points[i*sideSize+j+1], &Points[(i+1)*sideSize+j], 10000.d, 100.d);
		}
	}
}
