#include <cstring>
#include <ostream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>

#include <g3x.h>

#include "drapeau.hpp"

using namespace std;

Drapeau::Drapeau(unsigned short w, unsigned short h)
{
	this->w = w;
	this->h = h;
	
	this->p = h;
	this->m = w*h - h;
	this->l = w*h*w*h;
	
	init();
}

void Drapeau::init()
{
	if (this->Links == NULL)
		this->Links = new Link[l];
	if (this->PointsFixes == NULL)
		this->PointsFixes = new PointFixe[p];
	if (this->MassesLibres == NULL)
		this->MassesLibres = new MasseLibre[m];

    srand(24);

    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            G3Xpoint p = {(double)i,0.0,(double)j};
            Points[i*h + j].init(p);
        }
    }

    for (unsigned short i = 0; i < w; i++) {
        for (unsigned short j = 0; j < h-1; j++) {
            addLinks(i,j,i,j+1); //liasons horizontale
        }
    }

    for (unsigned short i = 0; i < w-1; i++) {
        for (unsigned short j = 0; j < h; j++) {
            addLinks(i,j,i+1,j); //liasons vertical
        }
    }

    for (unsigned short i = 0; i < w-1; i++) {
        for (unsigned short j = 0; j < h-1; j++) {
            addLinks(i,j,i+1,j+1); //liasons en croix
        }
    }

    for (unsigned short i = 0; i < w-1; i++) {
        for (unsigned short j = 1; j < h; j++) {
            addLinks(i,j,i+1,j-1); //liasons en croix
        }
    }

    for (unsigned short i = 0; i < w; i++) {
        for (unsigned short j = 0; j < h-2; j++) {
            addLinks(i,j,i,j+2); //liasons horizontal 2-2
        }
    }

    for (unsigned short i = 0; i < w-2; i++) {
        for (unsigned short j = 0; j < h; j++) {
            addLinks(i,j,i+2,j); //liasons vertical 2-2
        }
    }
}

void Drapeau::update()
{
    hook();
    hookGravity();
    hookWind();
    setup();
    autoCollision();
}

void Drapeau::computeBoundingBox(BoundingBox &b) {
#define _min(i) if(curMassePos[i] < b.min[i]) b.min[i] = curMassePos[i];
#define _max(i) if(curMassePos[i] > b.max[i]) b.max[i] = curMassePos[i];

    int nbMasses = p+m;
    if(nbMasses <= 0 ) return;
    b.min[0] = b.max[0] = Points[0].pos[0];
    b.min[1] = b.max[1] = Points[0].pos[1];
    b.min[2] = b.max[2] = Points[0].pos[2];
    for (int i = 0; i < nbMasses; i++) {
        G3Xpoint curMassePos = {Points[i].pos[0], Points[i].pos[1], Points[i].pos[2]};
        _min(0); _min(1); _min(2);
        _max(0); _max(1); _max(2);
    }
}

void Drapeau::splitBoundingBox(BoundingBox bb)
{
    //Clear old sub space
    subSpace.clear();

    //Build a set with index of all masses
    std::vector<int> set;
    for (int i = 0; i < p+m; i++) set.push_back(i);

    //Split the space
   _splitBoundingBox(0, bb, set);
}

void Drapeau::_splitBoundingBox(unsigned int deep,  BoundingBox bb, std::vector<int> set)
{
    /**
     * Split on x n times
     * Split each part on y o times
     * Split each part on z p times
     */

    BoundingBox b1 = bb;
    BoundingBox b2 = bb;

    if (deep < splitStep)
    {
        vector<int> sub1, sub2;
        for (vector<int>::iterator it = set.begin(); it != set.end(); it++) {
            if(Points[*it].pos[0] > (bb.max[0] / 2) )
                sub2.push_back(*it);
            else
                sub1.push_back(*it);
        }

        b1.max[0] = (bb.max[0]/2)+1;
        b2.min[0] = bb.max[0]/2;
        _splitBoundingBox(deep+1, b1, sub1);
        _splitBoundingBox(deep+1, b2, sub2);
    }
    else if (deep >= splitStep && deep < 2*splitStep)
    {
        vector<int> sub1, sub2;
        for (vector<int>::iterator it = set.begin(); it != set.end(); it++) {
            if(Points[*it].pos[1] > (bb.max[1] / 2) )
                sub2.push_back(*it);
            else
                sub1.push_back(*it);
        }

        b1.max[1] = (bb.max[1]/2)+1;
        b2.min[1] = bb.max[1]/2;
        _splitBoundingBox(deep+1, b1, sub1);
        _splitBoundingBox(deep+1, b2, sub2);
    }
    else
    {
        if (deep == 3*splitStep) {
            subSpace.push_back(set);
            return;
        }
        vector<int> sub1, sub2;
        for (vector<int>::iterator it = set.begin(); it != set.end(); it++) {
            if(Points[*it].pos[2] > (bb.max[2] / 2))
                sub2.push_back(*it);
            else
                sub1.push_back(*it);
        }

        b1.max[2] = (bb.max[2]/2)+1;
        b2.min[2] = bb.max[2]/2;

        _splitBoundingBox(deep+1, b1, sub1);
        _splitBoundingBox(deep+1, b2, sub2);
    }
}

std::vector<int>& Drapeau::getBoundingBox(int x, int y, int z) {

    static int l = pow(2, splitStep);
    static std::vector<int> set;

    if( x < 0 || y < 0 || z < 0 || x >= l || y >= l || z >= l ){
        set.empty();
        return set;
    }
    return subSpace[ (x * l) + (y * l * l) + z ];
}

void Drapeau::makeNeigbour(int x, int y, int z, std::vector<int> &m) {

    vector<int> set;

    set = getBoundingBox( x + (-1) , y + (-1),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (-1),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (-1),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (0),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (0),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (0),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (1),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (1),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (-1) , y + (1),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (-1),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (-1),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (-1),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (0),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (0),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (0),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (1),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (1),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (0) , y + (1),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (-1),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (-1),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (-1),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (0),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (0),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (0),  z + (1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (1),  z + (-1));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (1),  z + (0));
    m.insert(m.end(), set.begin(), set.end());

    set = getBoundingBox( x + (1) , y + (1),  z + (1));
    m.insert(m.end(), set.begin(), set.end());
}

void Drapeau::resolveNeigbourCollisions(int x, int y, int z)
{
    std::vector<int> &init = getBoundingBox(x,y,z);

    for (int i = -1; i < 2; ++i) {
        for (int j = -1; j < 2; ++j) {
            for (int k = -1; k < 2; ++k) {
                if(i == 0 && j == 0 && z == 0) continue;
                std::vector<int> &bb = getBoundingBox(x + i,y + j,z + k);
                if(bb.empty()) continue;
                resolveColision(init, bb);
            }
        }
    }
}

inline void Drapeau::resolveColision(std::vector<int> &set1, std::vector<int> &set2) {

    int l1 = set1.size();
    int l2 = set2.size();

    for (int i = 0; i < l1; i++) {
        for (int j = 0; j < l2; j++) {
            PMat *m1 = &Points[set1[i]], *m2 = &Points[set2[j]];
            if( !m1 || !m2 || m1 == m2 || m1->b[set2[j]] ) continue;

            double d = G3Xdist(m1->pos, m2->pos);
            if( d < 0.1 ) {
                G3Xvector m1m2 = {m2->pos[0] - m1->pos[0], m2->pos[1] - m1->pos[1], m2->pos[2] - m1->pos[2]};
                double _d = 1/d;
                G3Xvector fr = { (m1m2[0] * _d), (m1m2[1] * _d), (m1m2[2] * _d) };

                m1->pos[0] += fr[0];
                m1->pos[1] += fr[1];
                m1->pos[2] += fr[2];

                m2->pos[0] -= fr[0];
                m2->pos[1] -= fr[1];
                m2->pos[2] -= fr[2];
            }
            m1->b.set(set2[j]); m2->b.set(set1[i]);
        }
    }
}

void Drapeau::resolveColisionsQuad()
{
        std::vector<int> m;
        int size = h*w;
        for(int i = 0; i < size; i++) m.push_back(i);
        resolveColision(m, m);
}

void Drapeau::resolveCollisions()
{
    /*
     * For each bounding box :
     *  we check collisions beetween masses with neigbour box
     */

    //We check collisions on each box
    static int l = pow(2, splitStep);
    for (int i = 0; i < l; i++) {
        for (int j = 0; j < l; j++) {
            for (int k = 0; k < l; k++) {
                resolveColision(getBoundingBox(i,j,k), getBoundingBox(i,j,k));
            }
        }
    }

    //We check collision on each box with each neigbour
    for (int i = 0; i < l; i++) {
        for (int j = 0; j < l; j++) {
            for (int k = 0; k < l; k++) {
                resolveNeigbourCollisions(i,j,k);
            }
        }
    }
}

/**
 * @brief Drapeau::autoCollision
 *
 * - Conpute boundingBox
 * - Divides bounding box space to keep m masses max in each sub space
 * - Conpute collisions on each box and his neighbours
 */
void Drapeau::autoCollision()
{
#if 1
    //Conpute the global bounding arrond the flag
    BoundingBox bb;
    computeBoundingBox(bb);

    //Split the bounding on each axis
    splitBoundingBox(bb);

    //Reset flags on Pmat
    resetCollisions();

    //Resolve auto collision
    resolveCollisions();
#else
    resolveColisionsQuad();
#endif
}

void Drapeau::addLinks(unsigned short xM1, unsigned short yM1,
                       unsigned short xM2, unsigned short yM2)
{
    Links[((yM2*h+xM2)*w*h) + (yM1*h+xM1)].init(&Points[yM1*h+xM1], &Points[yM2*h+xM2], 10000.d, 100.d);
}
