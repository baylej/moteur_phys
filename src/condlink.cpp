#include "main.hpp"

CondLink::CondLink(Link *l, double ds) {
	init(l, ds);
}

void CondLink::init(Link *l, double ds) {
	this->ds = ds;
	this->link = l;
}

void CondLink::hook() {
	double dist = G3Xdist(link->m1->pos, link->m2->pos);
	if (dist < ds)
		link->hook();
}

void CondLink::draw() {
	link->draw();
}
