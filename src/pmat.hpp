#ifndef PMAT_H
#define PMAT_H
#pragma once

#include <bitset>
#include "main.hpp"

#define DRAPEAU_WITH 25
#define DRAPEAU_HEIGHT 25

/// Point materiel
class PMat
{
public:
    G3Xpoint  pos; // position de la masse
    G3Xvector vit; // vitesse
	G3Xvector frc; // resultante des forces
	double m;      // masse
    std::bitset<DRAPEAU_HEIGHT * DRAPEAU_WITH> b;
	
    PMat(G3Xpoint pos=def_v, double m=1.d);
	
	virtual void setup() = 0;
	void init(G3Xpoint pos, double m=1.d);
	
	// non physic
	double   ray; // rayon de la sphere
    G3Xcolor col; // couleur de la sphere
	
	void draw();
};

class MasseLibre: public PMat
{
public:
    MasseLibre(G3Xpoint pos=def_v, double m=1.d);
	
	void setup();
};

class PointFixe: public PMat
{
public:
    PointFixe(G3Xpoint pos=def_v);
	
	void setup();
};

#endif // PMAT_H
