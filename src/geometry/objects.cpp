#include "../main.hpp"
#include "objects.hpp"
#include "distimpl.hpp"

static void HMatAutoMult(G3Xhmat base, G3Xhmat multiplier)
{
	G3Xhmat tmp; G3Xcopymat(tmp, base);
	g3x_ProdHMat(tmp, multiplier, base);
}

static void create(Geometry*g, double rx, double ry, double rz, G3Xvector pos, double rotX, double rotY, double rotZ)
{
	G3Xhmat ope;
	g3x_MakeTranslationV(ope, pos);         HMatAutoMult(g->direct, ope);
	g3x_MakeRotationX(ope, rotX);           HMatAutoMult(g->direct, ope);
	g3x_MakeRotationY(ope, rotY);           HMatAutoMult(g->direct, ope);
	g3x_MakeRotationZ(ope, rotZ);           HMatAutoMult(g->direct, ope);
	g3x_MakeHomothetieXYZ(ope, rx, ry, rz); HMatAutoMult(g->direct, ope);

	G3Xmulvct(pos, -1.d);
	g3x_MakeHomothetieXYZ(ope, 1.d/rx, 1.d/ry, 1.d/rz); HMatAutoMult(g->inverse, ope);
	g3x_MakeRotationZ(ope, -rotZ);                      HMatAutoMult(g->inverse, ope);
	g3x_MakeRotationY(ope, -rotY);                      HMatAutoMult(g->inverse, ope);
	g3x_MakeRotationX(ope, -rotX);                      HMatAutoMult(g->inverse, ope);
	g3x_MakeTranslationV(ope, pos);                     HMatAutoMult(g->inverse, ope);
	G3Xmulvct(pos, -1.d);
}

Geometry* Objects::makeEllipse(double rx, double ry, double rz, G3Xvector pos, double rotX, double rotY, double rotZ)
{
	Geometry* g = new EucliDist();
	create(g, rx, ry, rz, pos, rotX, rotY, rotZ);
	return g;
}

Geometry* Objects::makeSquare(double rx, double ry, double rz, G3Xvector pos, double rotX, double rotY, double rotZ)
{
	Geometry* g = new CubicDist();
	create(g, rx, ry, rz, pos, rotX, rotY, rotZ);
	return g;
}

#include <iostream>
void Objects::test(Geometry *g)
{
	G3Xpoint p = {1.d, 1.d, 1.d};
	G3Xpoint q, y;
	g3x_ProdHMatPoint(g->direct,  p, q);
	g3x_ProdHMatPoint(g->inverse, q, y);
	if (y[0] != p[0] && y[1] != p[1] && y[2] != p[2]) {
		std::cout << "fail" << std::endl;
	} else {
		std::cout << "success" << std::endl;
	}
}
