#ifndef SHEET_HPP
#define SHEET_HPP

#include "object.hpp"

class Sheet : public Object
{
	int sideSize;
public:
	Sheet(int sideSize = 5);
	void init();
};

#endif // SHEET_HPP
