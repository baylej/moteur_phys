#ifndef LINK_H
#define LINK_H
#pragma once

/// Liaison
class Link
{
public:
	PMat *m1, *m2;
	double k, z, l;
	
    Link(PMat *m1=NULL, PMat *m2=NULL, double k=80000.d, double z=20000.d);
	
	void init(PMat *m1, PMat *m2, double k, double z);
	
	void hook();
	void draw();
};

#endif // LINK_H
