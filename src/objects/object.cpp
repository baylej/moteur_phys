#include "../main.hpp"

PMat& Object::_Points::operator [] (int i){
	if (i < parent->p)
		return parent->PointsFixes[i];
	else
		return parent->MassesLibres[i-parent->p];
}

Object::Object() {
	this->Points.parent = this;
	this->Links = NULL;
	this->PointsFixes = NULL;
	this->MassesLibres = NULL;
	this->p = this->m = this->l = 0;
}

void Object::translate(G3Xvector &t) {
	for (int i=0; i<p+m; i++)
		G3Xaddvct(Points[i].pos, t);
}

void Object::hook() {
	for (int i=0; i<l; i++)
		Links[i].hook();
}

void Object::hookGravity()
{
	for (int i=0; i<m; i++) {
		MassesLibres[i].frc[2] -= 400;
	}
}

void Object::hookWind()
{
    for (int i=0; i<m; i++) {
        MassesLibres[i].frc[0] += ((rand() % 1000)) / 5. + 550;
        MassesLibres[i].frc[1] += ((rand() % 1000)) / 100. ;
        MassesLibres[i].frc[2] += ((rand() % 1000) - 500) / 100000.;
    }
}

void Object::setup() {
	for (int i=0; i<p+m; i++)
		Points[i].setup();
}

void Object::update() {
	hook();
	hookGravity();
	hookWind();
	setup();
}

void Object::draw() {
	for (int i=0; i<p+m; i++)
		Points[i].draw();
	for (int i=0; i<l; i++)
        Links[i].draw();
}

void Object::resetCollisions()
{
    for (int i=0; i<p+m; i++)
        Points[i].b.reset();
}
