#ifndef OBJECT_H_
#define OBJECT_H_
#pragma once

#include "../main.hpp"

class Object {
public:
	int p, m, l;
	
	PointFixe    *PointsFixes;    // len = p
    MasseLibre   *MassesLibres;   // len = m
	Link         *Links;          // len = l
	
	/// une vue sur les tableaux PointsFixes et MassesLibres concatenes
	class _Points {
	public:
		Object *parent;
		PMat& operator [] (int);
	} Points; // len = p+m+v
	
	Object();
	
	/// deplace l'objet (agit directement sur le vecteur position)
	void translate(G3Xvector &t);
	/// calcule le systeme masses-ressort interne a l'objet
	void hook();
    /// calcule la gravité pour chaque point
    void hookGravity();
    /// calcule le vent pour chaque point
    void hookWind();
	/// a appeller apres hook
	void setup();
	/// appelle hook() puis setup()
	void update();
	/// dessine l'objet
	void draw();
    /// remet à zero les flags de collision
    void resetCollisions();
	
	/// cree les tableaux PointsFixes et MassesLibres et les liens eventuels
	virtual void init() = 0;
};

#endif /* OBJECT_H_ */
