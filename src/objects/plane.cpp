#include "../main.hpp"
#include "plane.hpp"

Plane::Plane(int sideSize, double gapSize): sideSize(sideSize), gapSize(gapSize)
{
	this->p = sideSize * sideSize;
	if (sideSize < 3) throw 1;
	init();
}

void Plane::init()
{
	if (this->PointsFixes == NULL)
		this->PointsFixes = new PointFixe[this->p];
	
	int d = sideSize / 2;
	
	for (int i=0; i<sideSize; i++) {
		for (int j=0; j<sideSize; j++) {
			G3Xpoint pnt = {(i-d)*gapSize, (j-d)*gapSize, 0.};
			this->PointsFixes[i*sideSize+j].init(pnt);
		}
	}
}
