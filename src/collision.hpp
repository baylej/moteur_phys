#ifndef COLLISION_HPP
#define COLLISION_HPP
#pragma once

#include "main.hpp"

/// Gere les collisions avec des condilinks
class Collision
{
	/// threshold pour les condilinks
	double thres;
	/// longeur du ressort a vide
	double linkLen;
	int l;
	CondLink *CondLinks; // len = l
	Object *a, *b;
public:
	/// Cree une gestion des collisions entre les 2 objets donnes
	Collision(Object *a, Object *b, double thres, double linkLen);
	
	/// calcule le systeme masses-ressort entre les 2 objets
	void hook();
	/// affiche les liens conditionnels (pour debug uniquement)
	void draw();
};

/// Gere les collisions avec les facettes
class FacetedCollision
{
	Object *a;
	Faceted *b;
public:
	double dissipation, rubbing;
	/// Cree une gestion des collisions entre un objet et un faceted.
	FacetedCollision(Object *a, Faceted *b, double dissipation, double rubbing);

	/// calcule les collisions entre l'object et le faceted
	/// cette methode doit IMPERATIVEMENT etre appelee entre le hook et le setup de l'ojbect
	void hook();
};

class GeometryCollision
{
	Object *a;
	Geometry *g;
public:
	GeometryCollision(Object *a, Geometry *g);

	/// Calcule les collisions conditionelles entre `a` et `g`
	void hook();
};

#endif // COLLISION_HPP
