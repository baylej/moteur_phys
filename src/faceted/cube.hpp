#ifndef CUBE_HPP
#define CUBE_HPP
#pragma once

#include "faceted.hpp"

class Cube : public Faceted
{
	double sizeSize;
public:
	Cube(double sideSize=1.d);
	void init();
};

#endif // CUBE_HPP
