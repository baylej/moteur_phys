#ifndef DISTIMPL_HPP
#define DISTIMPL_HPP
#pragma once

#include "geometry.hpp"

/// Une implementation de dist utilisant la distance euclidienne
class EucliDist : public Geometry
{
public:
	virtual double dist(G3Xpoint p1, G3Xpoint p2);
	virtual bool feedback(G3Xpoint p, G3Xvector f);
};

/// Une implementation de dist utilisant la distance cubique
class CubicDist : public Geometry
{
public:
	virtual double dist(G3Xpoint p1, G3Xpoint p2);
	virtual bool feedback(G3Xpoint p, G3Xvector f);
};

#endif // DISTIMPL_HPP
