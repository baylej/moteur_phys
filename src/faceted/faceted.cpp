#include "../main.hpp"

#include <cstring>
#include <iostream>

using std::memset;

Faceted::Faceted()
{
	this->f = 0;
	this->Facets = NULL;
	memset(this->facetColor, 0, sizeof(G3Xcolor));
}

void Faceted::draw()
{
	glColor4fv(this->facetColor);
	glBegin(GL_TRIANGLES);
	for (int i=0; i<this->f; i++) {
		glVertex3dv((GLdouble*)&this->Facets[i].a);
		glVertex3dv((GLdouble*)&this->Facets[i].b);
		glVertex3dv((GLdouble*)&this->Facets[i].c);
	}
	glEnd();
	glColor3f(1., 1., 1.);
	glLineWidth(2.5);
	for (int i=0; i<this->f; i++) {
		glBegin(GL_LINE_STRIP);
		glVertex3dv((GLdouble*)&this->Facets[i].a);
		glVertex3dv((GLdouble*)&this->Facets[i].b);
		glVertex3dv((GLdouble*)&this->Facets[i].c);
		glVertex3dv((GLdouble*)&this->Facets[i].a);
		glEnd();
	}
	glLineWidth(1.);
}

void Faceted::rotX(double alpha)
{
	G3Xpoint p;
	G3Xhmat rot; G3Xloadidentity(rot);
	g3x_MakeRotationX(rot, alpha);
	for (int i=0; i<this->f; i++) {
		G3Xcopy(p, this->Facets[i].a);
		g3x_ProdHMatPoint(rot, p, this->Facets[i].a);
		G3Xcopy(p, this->Facets[i].b);
		g3x_ProdHMatPoint(rot, p, this->Facets[i].b);
		G3Xcopy(p, this->Facets[i].c);
		g3x_ProdHMatPoint(rot, p, this->Facets[i].c);
	}
}

void Faceted::rotY(double alpha)
{
	G3Xpoint p;
	G3Xhmat rot; G3Xloadidentity(rot);
	g3x_MakeRotationY(rot, alpha);
	for (int i=0; i<this->f; i++) {
		G3Xcopy(p, this->Facets[i].a);
		g3x_ProdHMatPoint(rot, p, this->Facets[i].a);
		G3Xcopy(p, this->Facets[i].b);
		g3x_ProdHMatPoint(rot, p, this->Facets[i].b);
		G3Xcopy(p, this->Facets[i].c);
		g3x_ProdHMatPoint(rot, p, this->Facets[i].c);
	}
}
