#include "../main.hpp"
#include "fplane.hpp"

FacetedPlane::FacetedPlane(double sideSize)
{
	this->sideSize = sideSize;
	this->f = 2;
	init();
}

void FacetedPlane::init()
{
	if (this->Facets == NULL)
		this->Facets = new Facet[2];

	double d = this->sideSize/2.d;

	this->Facets[0].a[0] =  -d;
	this->Facets[0].a[1] =   d;
	this->Facets[0].a[2] = 0.d;

	this->Facets[0].b[0] =   d;
	this->Facets[0].b[1] =   d;
	this->Facets[0].b[2] = 0.d;

	this->Facets[0].c[0] =   d;
	this->Facets[0].c[1] =  -d;
	this->Facets[0].c[2] = 0.d;


	this->Facets[1].a[0] =  -d;
	this->Facets[1].a[1] =   d;
	this->Facets[1].a[2] = 0.d;

	this->Facets[1].b[0] =   d;
	this->Facets[1].b[1] =  -d;
	this->Facets[1].b[2] = 0.d;

	this->Facets[1].c[0] =  -d;
	this->Facets[1].c[1] =  -d;
	this->Facets[1].c[2] = 0.d;


	this->facetColor[0] = .27d;
	this->facetColor[1] = .31d;
	this->facetColor[2] = .43d;
}
