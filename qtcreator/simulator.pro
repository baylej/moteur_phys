TEMPLATE = app
CONFIG += console warn_on
CONFIG -= app_bundle qt

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    ../src/link.cpp \
    ../src/main.cpp \
    ../src/pmat.cpp \
    ../src/objects/drapeau.cpp \
    ../src/objects/object.cpp \
    ../src/condlink.cpp \
    ../src/collision.cpp \
    ../src/objects/plane.cpp \
    ../src/objects/sheet.cpp \
    ../src/faceted/faceted.cpp \
    ../src/faceted/fplane.cpp \
    ../src/faceted/cube.cpp \
    ../src/geometry/geometry.cpp \
    ../src/geometry/distimpl.cpp \
    ../src/geometry/objects.cpp

HEADERS += \
    ../src/main.hpp \
    ../src/link.hpp \
    ../src/pmat.hpp \
    ../src/objects/drapeau.hpp \
    ../src/objects/object.hpp \
    ../src/condlink.hpp \
    ../src/collision.hpp \
    ../src/objects/plane.hpp \
    ../src/objects/sheet.hpp \
    ../src/faceted/faceted.hpp \
    ../src/faceted/fplane.hpp \
    ../src/faceted/cube.hpp \
    ../src/geometry/geometry.hpp \
    ../src/geometry/distimpl.hpp \
    ../src/geometry/objects.hpp

LIBS = \
    -lm \
    -lGL \
    -lGLU \
    -lglut \
    -lg3x.64b
