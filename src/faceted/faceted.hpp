#ifndef FACETED_HPP
#define FACETED_HPP
#pragma once

#include "../main.hpp"

/// Une facette
class Facet
{
public:
	G3Xpoint a, b, c;
};

/// Un objet facette
class Faceted
{
public:
	int f;
	Facet *Facets; // len = f
	G3Xcolor facetColor;
public:
	/// Constructeur, initialise les champs (pointeurs mis à NULL)
	Faceted();

	/// Initialise l'objet facette
	virtual void init() = 0;

	/// Dessine les facettes
	void draw();
	/// Rotation des facettes en X
	void rotX(double alpha=PI_8);
	/// Rotation des facettes en Y
	void rotY(double alpha=PI_8);
};

#endif // FACETED_HPP
