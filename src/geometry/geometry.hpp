#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP
#pragma once

#include "../main.hpp"

/// Point Fixe Geometrique
class Geometry
{
public:
	/// Matrices de tranfo directe et inverse
	G3Xhmat direct, inverse;
	/// Constructeur, initialise les matrices à l'identite
	Geometry();

	/// Fonction de distance pour ce point
	virtual double dist(G3Xpoint p1, G3Xpoint p2) = 0;
	/// Calcule le feedback `f` a appliquer a `p` pour qu'il ne soit plus en collision
	virtual bool feedback(G3Xpoint p, G3Xvector f) = 0;

	/// Dessine le point fixe geometrique
	void draw();
};

#endif // GEOMETRY_HPP
