﻿#ifndef DRAPEAU_HPP
#define DRAPEAU_HPP
#pragma once

#include <vector>

#include "../main.hpp"

#define EPSILON 0.0000001

struct BoundingBox {
    G3Xpoint min, max;
};

class Drapeau: public Object
{
	int w, h;
public:
    Drapeau(unsigned short width = DRAPEAU_WITH, unsigned short height = DRAPEAU_HEIGHT);
	void init();

    /// Compute auto-collision
    void update();
    void autoCollision();
    void computeBoundingBox(BoundingBox&);
    void splitBoundingBox(BoundingBox);
    void _splitBoundingBox(unsigned int deep,  BoundingBox bb, std::vector<int> set);
    void resolveCollisions();
    std::vector<int>& getBoundingBox(int x, int y, int z);
    void makeNeigbour(int x, int y, int z, std::vector<int> &m);
    void resolveNeigbourCollisions(int x, int y, int z);
    void resolveColision(std::vector<int> &set1, std::vector<int> &set2);
    void resolveColisionsQuad();

    const unsigned int splitStep = 3;
    std::vector<std::vector<int>> subSpace;

    void addLinks(unsigned short xM1, unsigned short yM1, unsigned short xM2, unsigned short yM2);
};

#endif // DRAPEAU_HPP
