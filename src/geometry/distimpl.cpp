#include "../main.hpp"
#include "distimpl.hpp"
#include <cmath>

double EucliDist::dist(G3Xpoint p1, G3Xpoint p2)
{
	return G3Xdist(p1, p2);
}

using std::sqrt;
bool EucliDist::feedback(G3Xpoint p, G3Xvector f)
{
	G3Xzero(f);
	double d = sqrt(SQR(p[0]) + SQR(p[1])+ SQR(p[2]));

	if (d>1.d) return false;

	d = 1.d / sqrt(d) - 1.d;
	f[0] = p[0]*d;
	f[1] = p[1]*d;
	f[2] = p[2]*d;

	return true;
}

// ------

using std::abs;
double CubicDist::dist(G3Xpoint p1, G3Xpoint p2)
{
	double v0 = abs(p2[0] - p1[0]);
	double v1 = abs(p2[1] - p1[1]);
	double v2 = abs(p2[2] - p2[2]);

	return MAX3(v0, v1, v2);
}

bool CubicDist::feedback(G3Xpoint p, G3Xvector f)
{
	G3Xzero(f);
	double d = MAX3(abs(p[0]), abs(p[1]), abs(p[2]));

	if (d>1.d) return false;

	if (d == abs(p[0]))
		f[0] = p[0] * (1.d/d - 1.d);
	else if (d == abs(p[1]))
		f[1] = p[1] * (1.d/d - 1.d);
	else
		f[2] = p[2] * (1.d/d - 1.d);

	return true;
}
