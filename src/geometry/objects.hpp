#ifndef OBJECTS_HPP
#define OBJECTS_HPP
#pragma once

#include "geometry.hpp"

/// Cree des objets bases sur le point fixe geometrique
class Objects
{
public:
	/// Cree une ellipse avec les parametres donnes
	static Geometry* makeEllipse(double rx, double ry, double rz, G3Xvector pos, double rotX, double rotY, double rotZ);
	/// Cree un carre avec les parametres donnes
	static Geometry* makeSquare (double rx, double ry, double rz, G3Xvector pos, double rotX, double rotY, double rotZ);

	static void test(Geometry *g);
};

#endif // OBJECTS_HPP
