#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <g3x.h>

#include "main.hpp"

#include "objects/drapeau.hpp"
#include "objects/plane.hpp"
#include "objects/sheet.hpp"

#include "faceted/fplane.hpp"
#include "faceted/cube.hpp"

#include "geometry/objects.hpp"

unsigned int pixwidth=800, pixheight=600;

#ifdef __DT
double dt = __DT;
#else
double dt = .001d;
#endif
G3Xvector def_v = {0.d, 0.d, 0.d};

bool doAnim = false;
void setDoAnim(void) { doAnim = true; }

//--          Auto-Collisions          --
Drapeau dp;

void anim_ac(void)
{
	dp.update();
}

void dessin_ac(void)
{
	if (doAnim) { doAnim = false; anim_ac(); }
	g3x_Axis();
	dp.draw();
}

void reinit_ac(void) {
	dp.init();
}

void init_ac(void)
{
    glDisable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);

	G3Xpoint position = {13,-50,13};
    G3Xpoint target = {13,0,13};
    g3x_SetCameraCartesian(position, target);

	g3x_SetKeyAction(':', setDoAnim, (char*)"");
	g3x_SetKeyAction('!', reinit_ac, (char*)"");
}

//--   Collisions links conditionels   --
Plane p(27, .5);
Sheet s(11);
Collision c(&p, &s, .75, 1.);

void anim_cc(void)
{
	s.hook();
	s.hookGravity();
	c.hook();
	s.setup();
}

void dessin_cc(void)
{
	if (doAnim) { doAnim = false; anim_cc(); }
	g3x_Axis();
	p.draw();
	s.draw();
}

void reinit_cc(void) {
	p.init();
	s.init();
	
	G3Xvector trans = {0.d, 0.d, 10.d};
	s.translate(trans);
}

void init_cc(void)
{
	glDisable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);

	G3Xpoint position = {18, 18, 18};
    G3Xpoint target = {0, 0, 0};
    g3x_SetCameraCartesian(position, target);

	g3x_SetKeyAction(':', setDoAnim, (char*)"");
	g3x_SetKeyAction('!', reinit_cc, (char*)"");

	G3Xvector trans = {0.d, 0.d, 10.d};
	s.translate(trans);
}

//--    Collisions Dynamique inverse   --
Faceted *fo;
FacetedCollision *fc;

void anim_di(void)
{
	s.hook();
	s.hookGravity();
	fc->hook();
	s.setup();
}

void dessin_di(void)
{
	if (doAnim) { doAnim = false; anim_di(); }
	g3x_Axis();
	fo->draw();
	s.draw();
}

void reinit_di(void) {
	s.init();

	G3Xvector trans = {0.d, 0.d, 10.d};
	s.translate(trans);
}

void doRotX()
{
	fo->rotX();
}

void doRotY()
{
	fo->rotY();
}

void init_di(void)
{
	glDisable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LINE_SMOOTH);

	G3Xpoint position = {18, 18, 18};
	G3Xpoint target = {0, 0, 0};
	g3x_SetCameraCartesian(position, target);

	g3x_SetKeyAction(':', setDoAnim, (char*)"");
	g3x_SetKeyAction('!', reinit_di, (char*)"");

	g3x_SetKeyAction('c', doRotX, (char*)"");
	g3x_SetKeyAction('v', doRotY, (char*)"");

	G3Xvector trans = {0.d, 0.d, 10.d};
	s.translate(trans);
}
//-- Collisions Point Fixe Geometrique --
Geometry *g;
GeometryCollision *gc;

void anim_cpf(void)
{
	s.hook();
	s.hookGravity();
	gc->hook();
	s.setup();
}

void dessin_cpf(void)
{
	if (doAnim) { doAnim = false; anim_cpf(); }
	g3x_Axis();
	s.draw();
	g->draw();
}

void reinit_cpf(void)
{
	s.init();

	G3Xvector trans = {0.d, 0.d, 10.d};
	s.translate(trans);
}

void init_cpf(void)
{
	glDisable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LINE_SMOOTH);

	G3Xpoint position = {18, 18, 18};
	G3Xpoint target = {0, 0, 0};
	g3x_SetCameraCartesian(position, target);

	g3x_SetKeyAction(':', setDoAnim, (char*)"");
	g3x_SetKeyAction('!', reinit_cpf, (char*)"");

	G3Xvector trans = {0.d, 0.d, 10.d};
	s.translate(trans);
}

//--               Main                --
int main(int argc, char *argv[])
{
    g3x_InitWindow(*argv,pixwidth,pixheight);

    g3x_SetPerspective(40.,100.,1.);

	if (argc == 2 && strcmp(argv[1], "collision") == 0) {
		g3x_SetInitFunction(init_cc);
		g3x_SetAnimFunction(anim_cc);
		g3x_SetDrawFunction(dessin_cc);
	}
	else if(argc>=2 && strcmp(argv[1], "dyninv") == 0) {
		if (argc==3 && strcmp(argv[2], "cube") == 0)
			fo = new Cube(8.d);
		else
			fo = new FacetedPlane(20);
		fc = new FacetedCollision(&s, fo, .5d, .5d);
		g3x_SetInitFunction(init_di);
		g3x_SetAnimFunction(anim_di);
		g3x_SetDrawFunction(dessin_di);

		// Les scrolls doivent etres crees avant g3x_MainStart !
		g3x_CreateScrollv_d("d", &(fc->dissipation), 0.d, 1.d, .05, "dissipation");
		g3x_CreateScrollv_d("f",  &(fc->rubbing),     0.d, 1.d, .05, "frottement");
	}
	else if (argc >= 2 && strcmp(argv[1], "cpf") == 0) {
		G3Xpoint pos = {0.d, 0.d, 0.d};
		if (argc==3 && strcmp(argv[2], "cube") == 0) 
			g = Objects::makeSquare(4.d, 4.d, 4.d, pos, PI_4, PI_4, 0.d);
		else
			g = Objects::makeEllipse(5.d, 5.d, 5.d, pos, 0.d, 0.d, 0.d);
		Objects::test(g);
		gc = new GeometryCollision(&s, g);
		g3x_SetInitFunction(init_cpf);
		g3x_SetAnimFunction(anim_cpf);
		g3x_SetDrawFunction(dessin_cpf);
	}
	else {
		g3x_SetInitFunction(init_ac);
		g3x_SetAnimFunction(anim_ac);
		g3x_SetDrawFunction(dessin_ac);
	}

    return g3x_MainStart();
}
