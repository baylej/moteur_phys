#ifndef CONDLINK_HPP
#define CONDLINK_HPP
#pragma once

#include "main.hpp"

/// Un Link qui est effectif que si la dist entre m1 et m2 est inf a un seuil donne
class CondLink
{
	double ds; // ds = distance seuil
	Link *link;
public:
	CondLink(Link *l=NULL, double ds=0.1);
		
	void init(Link *l, double ds);
	void hook();
	void draw();
};

#endif // CONDLINK_HPP
